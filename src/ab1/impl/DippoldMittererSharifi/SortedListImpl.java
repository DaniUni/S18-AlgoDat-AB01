package ab1.impl.DippoldMittererSharifi;

import ab1.SortedList;

/**
 * Implementierung von SortedList als einfach verkettete Liste.
 *
 * @author Daniel Dippold, Felix Mitterer
 */
public class SortedListImpl implements SortedList
{
    private class ListElement
    {
        int key;
        ListElement next;

        ListElement(int key, ListElement next)
        {
            this.key = key;
            this.next = next;
        }

        ListElement(int key)
        {
            this(key, null);
        }
    }

    private ListElement firstElement;
    private int length;

    /**
     * Konstruktor.
     */
    public SortedListImpl()
    {
        firstElement = null;
        length = 0;
    }

    @Override
    public void clear()
    {
        firstElement = null;
        length = 0;
    }

    @Override
    public void insert(int key) throws IllegalArgumentException
    {
        //Case 1: Empty list
        if(firstElement == null) firstElement = new ListElement(key, null);

        //Case 2: Before first element
        else if(firstElement.key>key) firstElement = new ListElement(key, firstElement);

        //Case 3: Equal to first element
        else
            label: if(firstElement.key == key) throw new IllegalArgumentException();

        //Case 4: Go through list
        else {
            ListElement curr = firstElement.next;
            ListElement prev = firstElement;
            while (curr != null){
                //Case 1: Already in list
                if(curr.key == key) throw new IllegalArgumentException();

                //Case 2: Found larger element; insert between previous and current
                else if(curr.key>key){
                    prev.next = new ListElement(key, curr);
                    break label;

                //Case 3: Not found; prepare next iteration
                } else {
                    prev = prev.next;
                    curr = curr.next;
                }
            }
            //No larger element found; insert at end
            prev.next = new ListElement(key, null);
        }

        //Increase length if no exception above
        length++;
    }

    @Override
    public boolean remove(int key)
    {
        //List too short
        if(length == 0) return false;
        if(length == 1 && firstElement.key != key) return false;

        //Remove first element
        if(firstElement.key == key){
            firstElement = firstElement.next;
            length--;
            return true;
        }

        ListElement curr = firstElement.next;
        ListElement prev = firstElement;
        while (curr != null && curr.key <= key){
            if(curr.key == key){
                prev.next = curr.next;
                length--;
                return true;
            }
            prev = prev.next;
            curr = curr.next;
        }
        return false;
    }

    @Override
    public int getLength()
    {
        return length;
    }

    @Override
    public int[] toArray()
    {
        int[] iArray = new int[getLength()];
        ListElement curr = firstElement;
        for(int i=0; i<iArray.length; i++){
            iArray[i] = curr.key;
            curr = curr.next;
        }
        return iArray;
    }
}
