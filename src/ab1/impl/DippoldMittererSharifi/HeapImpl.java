package ab1.impl.DippoldMittererSharifi;

import ab1.Heap;
import ab1.Sorter;

import java.util.NoSuchElementException;

/**
 * Heap-Implementierung, die auf einem Array mit begrenzter Kapazität operiert.
 * Implementiert auch HeapSort.
 *
 * @author N.N.
 */
public class HeapImpl implements Heap, Sorter
{
    private int[] heap;
    int size;

    /**
     * Konstruktor.
     * @param capacity gewünschte Kapazität des Heap (maximale Anzahl von Elementen).
     */
    public HeapImpl(int capacity)
    {
        clear(capacity);
    }
    private void clear(int capacity){
        heap = new int[capacity];
        size = 0;
    }

    @Override
    public void clear()
    {
        clear(heap.length);
    }

    @Override
    public void add(int key) throws IndexOutOfBoundsException
    {
        if(size == heap.length) throw new IndexOutOfBoundsException();
        heap[size] = key;
        bubbleUp(size);
        size++;
    }

    public void setHeap(int[] array){
        clear(array.length);
        heap = array;
        size = array.length;
    }

    @Override
    public int removeMax() throws NoSuchElementException
    {
        if(size == 0) throw new NoSuchElementException();

        int item = heap[0];
        heap[0] = heap[size-1];
        heap[size-1] = item;
        size--;
        bubbleDown(0);
        return item;
    }

    @Override
    public int max() throws NoSuchElementException
    {
        if(size == 0) throw new NoSuchElementException();
        return heap[0];
    }

    @Override
    public int size()
    {
        return size;
    }

    @Override
    public void sort(int[] array)
    {
        //import array
        setHeap(array);

        //heapify
        for(int i = getParentIndex(size-1); i >= 0; i--){
            bubbleDown(i);
        }

        //move max to back of current heap (not array, the heap size decreases each iteration)
        while (size>0){
            removeMax();
        }
    }

    private void bubbleUp(int index){
        while (hasParent(index) && parent(index) < heap[index]){
            swap(getParentIndex(index), index);
            index = getParentIndex(index);
        }
    }

    private void bubbleDown(int index){
        while (hasLeftChild(index)){
            int largerChildIndex = (hasRightChild(index) && right(index) > left(index)) ? getRightIndex(index) : getLeftIndex(index);
            if(heap[index] > heap[largerChildIndex]) break;
            else swap(index, largerChildIndex);
            index = largerChildIndex;
        }
    }


    private int getLeftIndex(int index){ return 2 * index + 1; }
    private int getRightIndex(int index){ return 2 * index + 2; }
    private int getParentIndex(int index){ return (index - 1) / 2; }

    private boolean hasLeftChild(int index){ return getLeftIndex(index) < size; }
    private boolean hasRightChild(int index){ return getRightIndex(index) < size; }
    private boolean hasParent(int index){ return getParentIndex(index) >= 0; }

    private int left(int index){ return heap[getLeftIndex(index)]; }
    private int right(int index){ return heap[getRightIndex(index)]; }
    private int parent(int index){ return heap[getParentIndex(index)]; }

    private void swap(int i1, int i2){
        int temp = heap[i1];
        heap[i1] = heap[i2];
        heap[i2] = temp;
    }

/*    private void printArray(){
        printArray(heap);
    }

    private void printArray(int[] array){
        System.out.print("[");
        for(int i=0; i<array.length; i++){
            String s = array[i] + "";
            s += (size-1 == i) ? ']' : ' ';
            System.out.print(s + ' ');
        }
        System.out.println("size: "+size);
    }*/

}
