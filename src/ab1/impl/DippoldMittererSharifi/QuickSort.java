package ab1.impl.DippoldMittererSharifi;

import ab1.Sorter;

/**
 * QuickSort-Implementierung der Sorter-Schnittstelle.
 * Verwendet das mittlere Element eines Intervalls als Pivotelement.
 *
 * @author Daniel Dippold, Felix Mitterer
 */
public class QuickSort implements Sorter
{
    @Override
    public void sort(int[] array)
    {
        recursiveSort(array, 0, array.length-1);
    }

    private void recursiveSort(int[] array, int low, int high){
        if(low < high){
            int pivot = interval(array, low, high);

            recursiveSort(array, low, pivot-1);
            recursiveSort(array, pivot+1, high);
        }
    }

    private int interval(int[] array, int low, int high){
        int pivot = low + (high-low)/2;
        boolean switchLow = false;
        boolean switchHigh = false;

        while ( !(low >= pivot && high <= pivot) ) {
            if(switchLow == false && compareLow(array, low, pivot)) switchLow = true;
            if(switchHigh == false && compareHigh(array, pivot, high)) switchHigh = true;

            //Case 1: Low iterator and High iterator both found an element on the wrong side
            if(switchLow && switchHigh){
                swap(array, low, high);
                switchLow = switchHigh = false;
            }
            //Case 2: Low iterator found an element on the wrong side and High iterator caught up to pivot
            else if (switchLow && high == pivot){
                swap(array, low, pivot);
                pivot = low;
                switchLow = false;
            }
            //Case 2: High iterator found an element on the wrong side and Low iterator caught up to pivot
            else if (switchHigh && low == pivot){
                swap(array, pivot, high);
                pivot = high;
                switchHigh = false;
            }

            //In- or decrease iterator if current element should not be switched and it hasn't caught up to the pivot
            if(!switchLow && low != pivot) low++;
            if(!switchHigh && high != pivot) high--;
        }
        return pivot;
    }

    private boolean compareLow(int[] array, int lowIndex, int pivotIndex){
        return array[lowIndex] > array[pivotIndex];
    }

    private boolean compareHigh(int[] array, int pivotIndex, int highIndex){
        return array[pivotIndex] > array[highIndex];
    }

    private void swap(int[] array, int index1, int index2){
        int temp = array[index1];
        array[index1] = array[index2];
        array[index2] = temp;
    }

}
